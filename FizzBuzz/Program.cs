﻿using System;
using System.Collections.Generic;
using FizzBuzzLib;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new List<Config>
            {
                new Config(3, "Fizz"),
                new Config(5, "Buzz"),
                new Config(10, "Bazz")
            };

            var runner = new FizzBuzzer(config);

            foreach(var result in runner.Run(1, 100))
            {
                Console.Write(result);
            }
            Console.ReadKey();
        }
    }
}
