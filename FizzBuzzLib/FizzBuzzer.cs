﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzzLib
{
    public class FizzBuzzer
    {
        private List<Config> Configs { get; }

        public FizzBuzzer(List<Config> configs)
        {
            if(configs == null)
                throw new ArgumentNullException(nameof(configs));
            Configs = configs;
        }

        public IEnumerable<string> Run(int start, int end)
        {
            if(end < start)
                throw new Exception("Start value must be less than end value");

            for (var i = start; i <= end; i++)
            {
                if (!Configs.Matching(i).Any())
                {
                    yield return $"{i}\n";
                    continue;
                }

                foreach (var config in Configs.Matching(i).OrderBy(c => c.Divisor))
                    yield return config.Word;
                
                yield return "\n";
            }
        } 
    }
}
