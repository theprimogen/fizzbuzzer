﻿using System;

namespace FizzBuzzLib
{
    public class Config
    {
        public Config(uint divisor, string word)
        {
            if(divisor == 0)
                throw new ArgumentOutOfRangeException(nameof(divisor));

            Divisor = divisor;
            Word = word;
        }

        public uint Divisor { get; set; }
        public string Word { get; set; }

        public bool IsMatch(int value)
        {
            return value%Divisor == 0;
        }
    }
}
