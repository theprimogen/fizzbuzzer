﻿using System.Collections.Generic;
using System.Linq;

namespace FizzBuzzLib
{
    public static class Extensions
    {
        public static IEnumerable<Config> Matching(this IEnumerable<Config> configs, int value)
        {
            return configs.Where(c => c.IsMatch(value));
        }
    }
}
