﻿using System;
using NUnit.Framework;
using FizzBuzzLib;
using Shouldly;

namespace FizzBuzzTests
{
    [TestFixture]
    public class ConfigTests
    {
        [Test]
        public void Should_throw_exception_for_0_divisor()
        {
            Should.Throw<ArgumentOutOfRangeException>(() => new Config(0, "Foo"));
        }

        [Test]
        public void Should_return_true_when_matched()
        {
            var config = new Config(3, "Foo");
            config.IsMatch(9).ShouldBeTrue();
        }

        [Test]
        public void Should_return_false_when_not_match()
        {
            var config = new Config(7, "Foo");
            config.IsMatch(9).ShouldBeFalse();
        }
    }
}
