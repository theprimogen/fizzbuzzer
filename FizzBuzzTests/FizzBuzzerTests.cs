﻿using System;
using FizzBuzzLib;
using NUnit.Framework;
using Shouldly;

namespace FizzBuzzTests
{
    [TestFixture]
    public class FizzBuzzerTests
    {
        [Test]
        public void Should_throw_exception_when_config_normal()
        {
            Should.Throw<ArgumentNullException>(() => new FizzBuzzer(null));
        }
    }
}
