﻿using System.Linq;
using FizzBuzzLib;
using NUnit.Framework;
using Shouldly;

namespace FizzBuzzTests
{
    public class FizzBuzzTests
    {
        private static string TestValue(int value, params Config[] configs)
        {
            var runner = new FizzBuzzer(configs.ToList());
            return string.Join("", runner.Run(value, value));
        }

        [Test]
        public void When_two_values_match_Should_return_both_words()
        {
            var result = TestValue(15, new Config(3, "Fizz"), new Config(5, "Buzz"));
            result.ShouldBe("FizzBuzz\n");
        }

        [Test]
        public void When_no_value_match_should_return_value()
        {
            var result = TestValue(7, new Config(3, "Fizz"), new Config(5, "Buzz"));
            result.ShouldBe("7\n");
        }
    }
}
